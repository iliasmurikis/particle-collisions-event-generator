import warnings
from math import atan, sqrt
import numpy as np

warnings.filterwarnings("ignore", category=RuntimeWarning)


def momentums_to_angles(px, py, pz):
    def calculate_phi(x, y):
        if x > 0:
            phi = np.arctan(y / x)
        elif x < 0 and y >= 0:
            phi = np.arctan(y / x) + np.deg2rad(180)
        elif x < 0 and y < 0:
            phi = np.arctan(y / x) - np.deg2rad(180)
        elif x == 0 and y > 0:
            phi = np.deg2rad(180 / 2)
        elif x == 0 and y < 0:
            phi = -np.deg2rad(180 / 2)
        else:
            phi = None

        return phi

    if type(px).__module__ == np.__name__ and type(py).__module__ == np.__name__ and type(pz).__module__ == np.__name__:
        phi = np.array([calculate_phi(px[i], py[i]) for i in range(len(px))])
        theta = np.arctan(np.divide(np.sqrt(np.add(np.power(px, 2), np.power(py, 2))), pz))
        r = np.sqrt(np.add(np.add(np.power(px, 2), np.power(py, 2)), np.power(pz, 2)))
    else:
        phi = calculate_phi(px, py)
        theta = atan(sqrt(px ** 2 + py ** 2) / pz)
        r = sqrt(px ** 2 + py ** 2 + pz ** 2)

    return phi, theta, r


def angles_to_momentums(phi, theta, r):
    return [r * np.cos(phi) * np.sin(theta),
            r * np.sin(phi) * np.sin(theta),
            r * np.cos(theta)]
