from math import sqrt
import numpy as np

from convertors import momentums_to_angles, angles_to_momentums


def sample_momentum_vectors(E_tot, M):
    # X1
    e = np.random.normal(E_tot/2, 10)
    m = np.random.uniform(0, M)
    dif = e**2 - m**2
    p_x = np.random.uniform(-sqrt(dif), sqrt(dif))
    dif -= p_x**2
    p_y = np.random.uniform(-sqrt(dif), sqrt(dif))
    dif -= p_y ** 2
    p_z = sqrt(dif)

    # X2
    dif2 = (E_tot-e)**2 - m**2
    p_x2 = np.random.uniform(-sqrt(dif2), sqrt(dif2))
    dif2 -= p_x2**2
    p_y2 = np.random.uniform(-sqrt(dif2), sqrt(dif2))
    dif2 -= p_y2**2
    p_z2 = sqrt(dif2)

    return e, m, p_x, p_y, p_z, p_x2, p_y2, p_z2


def generate_dataset(E_tot, M, num_of_samples):
    dataset = []
    for _ in range(num_of_samples):
        # Create X1 particle and X2 anti-particle 3-momentum vectors
        e, m, p_x, p_y, p_z, p_x2, p_y2, p_z2 = sample_momentum_vectors(E_tot, M)

        dataset.append([e, p_x, p_y, p_z, m, E_tot-e, p_x2, p_y2, p_z2, m])

    return np.asarray(dataset)


def smear_energy(dataset, num_of_samples):
    # Apply 10% smearing
    dataset[:, 0] = np.multiply(dataset[:, 0], np.random.normal(1, 0.1, num_of_samples))
    dataset[:, 5] = dataset[:, 0]

    return dataset


def smear_angles(dataset):
    # Convert momentums to spherical coordinate system
    phi, theta, r = momentums_to_angles(dataset[:, 1], dataset[:, 2], dataset[:, 3])

    # Apply 5% smearing
    phi = np.multiply(phi, np.random.normal(1, 0.05))
    theta = np.multiply(theta, np.random.normal(1, 0.05))

    # Convert spherical coordinate system to momentums
    p_x, p_y, p_z = angles_to_momentums(phi, theta, r)

    dataset[:, 1] = p_x
    dataset[:, 2] = p_y
    dataset[:, 3] = p_z
    dataset[:, 6] = np.multiply(p_x, -1)
    dataset[:, 7] = np.multiply(p_y, -1)
    dataset[:, 8] = np.multiply(p_z, -1)

    return dataset
